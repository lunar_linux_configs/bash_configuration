#     __                           __  ___           __
#    / /   __  ______  ____ ______/  |/  /___ ______/ /__
#   / /   / / / / __ \/ __ `/ ___/ /|_/ / __ `/ ___/ //_/
#  / /___/ /_/ / / / / /_/ / /  / /  / / /_/ (__  ) ,<
# /_____/\__,_/_/ /_/\__,_/_/  /_/  /_/\__,_/____/_/|_|_____
#                                                     /____/
## BASHRC by Lunarmask_

# Perform this command to apply this config
# $ ln ~/.config/bashrc ~/.bashrc

# ---------------------
#    BASH SETTINGS
# ---------------------

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# Append to the history file, don't overwrite it
shopt -s histappend

# For setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=100000
HISTTIMEFORMAT="%F %T: "

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# ---------------------
#   INITIALIZE PATHS
# ---------------------

export XDG_CONFIG_HOME="$HOME/.config"

# ---------------------
#  DIRECTORIES ALIASES
# ---------------------

export FDIR="$XDG_CONFIG_HOME/fish"
export BDIR="$XDG_CONFIG_HOME/bash"
export ADIR="$XDG_CONFIG_HOME/alacritty"
export TDIR="$XDG_CONFIG_HOME/tmux"
export NDIR="$XDG_CONFIG_HOME/nvim"
export PDIR="$XDG_CONFIG_HOME/personal"
export VFDIR="$XDG_CONFIG_HOME/vifm"
export FEDIR="$XDG_CONFIG_HOME/feh"

# ---------------------
#  DIRECTORIES JUMPING
# ---------------------

alias gdow="cd ~/Downloads"
alias gdoc="cd ~/Documents"
alias grb="~/.ruby_scripts"
alias gex="~/.elixir_scripts"

# ---------------------
#    CONFIG ALIASES
# ---------------------

export FRC="$FDIR/config.fish"
export FA="$FDIR/alias.fish"
export BRC="$BDIR/bashrc"
export BA="$BDIR/alias.bash"
export ZRC="$HOME/.zshrc"

export ARC="$ADIR/alacritty.yml"
export TRC="$TDIR/tmux.conf"
export NRC="$NDIR/init.vim"
export FEK="$FEDIR/feh/keys"
export FET="$FEDIR/feh/themes"
export VFRC="$FMDIR/vifmrc"

# ---------------------
#      ENV SETTERS
# ---------------------

# Change this to proper machine name
export MACHINE="home_desktop"

export SHELL=$(which bash)
export EDITOR="nvim"
export VISUAL="nvim"
export BROWSER="firefox"
export TERMINAL="alacritty"
export FZF_CTRL_T_COMMAND="command find -L \$dir -type f 2> /dev/null | sed '1d; s#^\./##'"

[[ -x "/usr/bin/bat" ]] && export MANPAGER "sh -c 'col -bx | bat -l man -p'"

[[ -f "$ADIR/active_colorscheme" ]] && export COLORSCHEME=$(cat "$ADIR/active_colorscheme")
[[ -f "$ADIR/current_font" ]] && export FONTSET=$(cat "$ADIR/current_font")

# ---------------------
#  Additional Sourcing
# ---------------------

[[ -f "$BDIR/alias.bash" ]] && source "$BDIR/alias.bash"
[[ -f "$PDIR/alias.bash" ]] && source "$PDIR/alias.bash"

# ---------------------
#    COMMAND PROMPT
# ---------------------

export PS1="\e[0;36m[$MACHINE] \e[0m\w \e[0;32m> \e[0m"

# ---------------------
#    BASH FUNCTIONS
# ---------------------

# ex - archive extractor
ex () {
  if [ -f $1 ] ; then
    case $1 in
      *.zip)       7z x $1    ;;
      *.7z)        7z x $1    ;;
      *.rar)       7z x $1    ;;
      *.tar)       tar xf $1  ;;
      *.tar.bz2)   tar xjf $1 ;;
      *.tar.gz)    tar xzf $1 ;;
      *.tbz2)      tar xjf $1 ;;
      *.tgz)       tar xzf $1 ;;
      *.bz2)       bunzip2 $1 ;;
      *.gz)        gunzip $1  ;;
      *.Z)         uncompress $1;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

