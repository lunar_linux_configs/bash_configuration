#     __                           __  ___           __
#    / /   __  ______  ____ ______/  |/  /___ ______/ /__
#   / /   / / / / __ \/ __ `/ ___/ /|_/ / __ `/ ___/ //_/
#  / /___/ /_/ / / / / /_/ / /  / /  / / /_/ (__  ) ,<
# /_____/\__,_/_/ /_/\__,_/_/  /_/  /_/\__,_/____/_/|_|_____
#                                                     /____/
## BASH_ALIAS by Lunarmask_

# Clipboard Managing
alias copy="xsel -i -b"
alias paste="xsel -o -b"
alias copy-select="xsel -i -p"
alias paste-select="xsel -o -p"

alias less="less -r"

# LS MAPPINGS
if [[ -x /usr/bin/exa ]]; then
  alias l="exa -aF --group-directories-first"
  alias ld="exa -aFD"
  alias ll="exa -lahF --group-directories-first"
  alias lld="exa -lahFD"
  alias llr="exa -lahF --sort=modified --reverse | head"
  alias lll="exa -lahF --group-directories-first | bat"
else
  alias l="ls -AF --color --group-directories-first"
  alias ld="ls -AF --color --group-directories-first | grep '/\$'"
  alias ll="ls -lAhF --color --group-directories-first"
  alias lld="ls -lAhF --color --group-directories-first | grep '/\$'"
  alias llr="ls -lAhFtc --color | head -10"
  alias lll="ls -lAhF --group-directories-first | less"
fi

# TMUX
alias t="tmux"
alias ta="tmux attach -t 0"
alias tls="tmux ls"
alias ts="tmux source-file ~/.config/tmux/tmux.conf"

# GIT
alias g="git"
alias ga="git add"
alias ga.="git add ."
alias gst="git status"
alias gbr="git branch"
alias gbra="git branch -a"
alias gco="git checkout"
alias gci="git commit"
alias gciv="git commit -v"
alias gd="git diff"
alias gp="git push origin HEAD"
alias glg="git log"
alias gum="git checkout master; and git fetch --prune; and git rebase"

# LINUX OS TOOLS
alias df="df -ah"
alias du="du -ch"
alias v="nvim"
alias nano="nano -liED"

alias cheat="curl cheat.sh/"

alias corona_stats="curl https://corona-stats.online/states/US\?minimal=true"

alias :q="exit"

