#!/bin/bash

# Ansi color code variables

# reset
res="\e[0m"

# Regular Colors
black="\e[0;30m"
red="\e[0;31m"
green="\e[0;32m"
yellow="\e[0;33m"
blue="\e[0;34m"
purple="\e[0;35m"
cyan="\e[0;36m"
white="\e[0;37m"

echo -e "${res}\n normal \n"

echo -e "${black} Sphinx of black quarts; judge my vow!"
echo -e "${red} Sphinx of black quarts; judge my vow!"
echo -e "${green} Sphinx of black quarts; judge my vow!"
echo -e "${yellow} Sphinx of black quarts; judge my vow!"
echo -e "${blue} Sphinx of black quarts; judge my vow!"
echo -e "${purple} Sphinx of black quarts; judge my vow!"
echo -e "${cyan} Sphinx of black quarts; judge my vow!"
echo -e "${white} Sphinx of black quarts; judge my vow!"

# Bold Colors
b_black="\e[1;30m"
b_red="\e[1;31m"
b_green="\e[1;32m"
b_yellow="\e[1;33m"
b_blue="\e[1;34m"
b_purple="\e[1;35m"
b_cyan="\e[1;36m"
b_white="\e[1;37m"

echo -e "${res}\n bold \n"

echo -e "${b_black} Sphinx of black quarts; judge my vow!"
echo -e "${b_red} Sphinx of black quarts; judge my vow!"
echo -e "${b_green} Sphinx of black quarts; judge my vow!"
echo -e "${b_yellow} Sphinx of black quarts; judge my vow!"
echo -e "${b_blue} Sphinx of black quarts; judge my vow!"
echo -e "${b_purple} Sphinx of black quarts; judge my vow!"
echo -e "${b_cyan} Sphinx of black quarts; judge my vow!"
echo -e "${b_white} Sphinx of black quarts; judge my vow!"

# High Intensty
bh_black="\e[0;90m"
bh_red="\e[0;91m"
bh_green="\e[0;92m"
bh_yellow="\e[0;93m"
bh_blue="\e[0;94m"
bh_purple="\e[0;95m"
bh_cyan="\e[0;96m"
bh_white="\e[0;97m"

# Bold High Intensty
Bh_black="\e[1;90m"
Bh_red="\e[1;91m"
Bh_green="\e[1;92m"
Bh_yellow="\e[1;93m"
Bh_blue="\e[1;94m"
Bh_purple="\e[1;95m"
Bh_cyan="\e[1;96m"
Bh_white="\e[1;97m"


red="\e[0;91m"
green="\e[0;92m"
gold="\3[0;93m"
blue="\e[0;94m"
blue="\e[0;95m"
blue="\e[0;96m"
blue="\e[0;97m"
blue="\e[0;98m"
blue="\e[0;99m"
white="\e[0;97m"
expand_bg="\e[K"
blue_bg="\e[0;104m${expand_bg}"
red_bg="\e[0;101m${expand_bg}"
green_bg="\e[0;102m${expand_bg}"
bold="\e[1m"
uline="\e[4m"
reset="\e[0m"

# horizontally expanded backgrounds
echo -e "${blue_bg}${reset}"
echo -e "${red_bg}${reset}"
echo -e "${green_bg}${reset}"

echo ""

# colored text
echo -e "${red}Hello World!${reset}"
echo -e "${blue}Hello World!${reset}"
echo -e "${green}Hello World!${reset}"
echo -e "${white}Hello World!${reset}"

echo ""

# bold colored text
echo -e "${red}${bold}Hello World!${reset}"
echo -e "${blue}${bold}Hello World!${reset}"
echo -e "${green}${bold}Hello World!${reset}"
echo -e "${white}${bold}Hello World!${reset}"

echo ""

# underlined colored text
echo -e "${red}${uline}Hello World!${reset}"
echo -e "${blue}${uline}Hello World!${reset}"
echo -e "${green}${uline}Hello World!${reset}"
echo -e "${white}${uline}Hello World!${reset}"

echo ""

# ansi across multiple lines
echo -e "${green}This is a sentence across"
echo "three lines to show how an ansi color"
echo -e "works across multiple lines with echo.${reset}"

echo ""

# combining ansi in one line
echo -e "${red}This sentence ${green}displays ${blue}ansi code used in ${white}${bold}combination.${reset}"
