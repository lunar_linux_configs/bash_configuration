#!/bin/bash

# Ansi color code variables

# reset
res="\e[0m"

# Regular Colors
black="\e[0;30m"
red="\e[0;31m"
green="\e[0;32m"
yellow="\e[0;33m"
blue="\e[0;34m"
purple="\e[0;35m"
cyan="\e[0;36m"
white="\e[0;37m"

echo -e "${res}\n normal \n"

echo -e "${black} Sphinx of black quarts; judge my vow!"
echo -e "${red} Sphinx of black quarts; judge my vow!"
echo -e "${green} Sphinx of black quarts; judge my vow!"
echo -e "${yellow} Sphinx of black quarts; judge my vow!"
echo -e "${blue} Sphinx of black quarts; judge my vow!"
echo -e "${purple} Sphinx of black quarts; judge my vow!"
echo -e "${cyan} Sphinx of black quarts; judge my vow!"
echo -e "${white} Sphinx of black quarts; judge my vow!"

# Bold Colors
b_black="\e[1;30m"
b_red="\e[1;31m"
b_green="\e[1;32m"
b_yellow="\e[1;33m"
b_blue="\e[1;34m"
b_purple="\e[1;35m"
b_cyan="\e[1;36m"
b_white="\e[1;37m"

echo -e "${res}\n bold \n"

echo -e "${b_black} Sphinx of black quarts; judge my vow!"
echo -e "${b_red} Sphinx of black quarts; judge my vow!"
echo -e "${b_green} Sphinx of black quarts; judge my vow!"
echo -e "${b_yellow} Sphinx of black quarts; judge my vow!"
echo -e "${b_blue} Sphinx of black quarts; judge my vow!"
echo -e "${b_purple} Sphinx of black quarts; judge my vow!"
echo -e "${b_cyan} Sphinx of black quarts; judge my vow!"
echo -e "${b_white} Sphinx of black quarts; judge my vow!"

# High Intensty
h_black="\e[0;90m"
h_red="\e[0;91m"
h_green="\e[0;92m"
h_yellow="\e[0;93m"
h_blue="\e[0;94m"
h_purple="\e[0;95m"
h_cyan="\e[0;96m"
h_white="\e[0;97m"

echo -e "${res}\n High Intensity \n"

echo -e "${h_black} Sphinx of black quarts; judge my vow!"
echo -e "${h_red} Sphinx of black quarts; judge my vow!"
echo -e "${h_green} Sphinx of black quarts; judge my vow!"
echo -e "${h_yellow} Sphinx of black quarts; judge my vow!"
echo -e "${h_blue} Sphinx of black quarts; judge my vow!"
echo -e "${h_purple} Sphinx of black quarts; judge my vow!"
echo -e "${h_cyan} Sphinx of black quarts; judge my vow!"
echo -e "${h_white} Sphinx of black quarts; judge my vow!"

# Bold High Intensty
bh_black="\e[1;90m"
bh_red="\e[1;91m"
bh_green="\e[1;92m"
bh_yellow="\e[1;93m"
bh_blue="\e[1;94m"
bh_purple="\e[1;95m"
bh_cyan="\e[1;96m"
bh_white="\e[1;97m"

echo -e "${res}\n High Intensity Bold \n"

echo -e "${bh_black} Sphinx of black quarts; judge my vow!"
echo -e "${bh_red} Sphinx of black quarts; judge my vow!"
echo -e "${bh_green} Sphinx of black quarts; judge my vow!"
echo -e "${bh_yellow} Sphinx of black quarts; judge my vow!"
echo -e "${bh_blue} Sphinx of black quarts; judge my vow!"
echo -e "${bh_purple} Sphinx of black quarts; judge my vow!"
echo -e "${bh_cyan} Sphinx of black quarts; judge my vow!"
echo -e "${bh_white} Sphinx of black quarts; judge my vow!"
